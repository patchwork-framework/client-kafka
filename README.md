# Patchwork Client Kafka

Kafka Client implementation for Patchwork - an asynchronous distributed microframework.
Currently, only asynchronous client is implemented.


## Requirements
* Python 3.7

## Installation

``pip install patchwork-client-kafka`

## Documentation

https://patchwork-framework.gitlab.io/core

