# Patchwork Kafka Client

Project source code can be found at [GitLab](https://gitlab.com/patchwork-framework/client-kafka).

Package name: `patchwork-client-kafka`

This package contains a client implementation for Kafka message broker.

## Asynchronous

::: patchwork.client.kafka.publisher
::: patchwork.client.kafka.subscriber

## Common
::: patchwork.client.kafka.common
